Dashboard = function(dashboardId, dashboardVersion, interval, items, defaultQueryHref, defaultQueryURL, defaultView, forceClearCachedValues) {
    var _this = this;

    var hash = window.location.hash || '' ;
    var pageName = hash.substring(0, hash.indexOf('?')).substring(hash.lastIndexOf('/')+1) || '';

    this.dashboardId = dashboardId;
    this.localStorageKey = pageName + '-' + dashboardId;
    this.dashboardVersion = dashboardVersion;
    this.items = items;
    this.interval = interval;
    this.defaultQueryURL = defaultQueryURL;
    this.defaultQueryHref = defaultQueryHref;
    this.defaultView = defaultView || 0;
    this.timeoutId = -1;

    //Se localstorage então actualiza valores com histórico
    //TODO: criar sistema de versionamento da definição de dashboard, para não estar a usar valores antigos numa nova definição

    var storedUser = localStorage && localStorage.getItem(this.localStorageKey+"username") || "";
    localStorage && localStorage.setItem(this.localStorageKey+"username",cob.app.getCurrentLoggedInUser());

    var storedVersion = localStorage && localStorage.getItem(this.localStorageKey+"version") || "";
    localStorage && localStorage.setItem(this.localStorageKey+"version",this.dashboardVersion);

    var shouldGetCachedValues = (!forceClearCachedValues && storedUser == cob.app.getCurrentLoggedInUser() && storedVersion == this.dashboardVersion);

    if(shouldGetCachedValues) {
        var storedValues = localStorage.getItem(this.localStorageKey+'Values');
        this.values = localStorage && storedValues && JSON.parse(storedValues) || [];
    } else {
        this.clearCachedValues();
    }

    this.buildHtml();
    this.updateAllItems();
    this.launchUpdateDataCycle();

    //Parar de fazer update quando se sai da página actual
    $(document).unload(function(){
        _this.stopUpdateCycle();
    });
};

Dashboard.prototype.clearCachedValues = function() {
    localStorage && localStorage.setItem(this.localStorageKey+'Values',"") ;
    this.values = [];
};

Dashboard.prototype.updateAllItems = function() {
    for(var itemIndex=0; itemIndex < this.items.length; itemIndex++){
        this.updateItem(itemIndex);
    }
};

Dashboard.prototype.launchUpdateDataCycle = function() {
    //se invisível ignorar todo o código pois já saímos do dashboard
    if($('#'+this.dashboardId).length) {
        var now = Date.now();
        var me = this;

        //actualiza hora do nextUpdate com valor global ao browser
        this.nextUpdate = localStorage && localStorage.getItem(this.localStorageKey+"NextUpdate") || 0;
        var storedValues = localStorage && localStorage.getItem(this.localStorageKey+"Values");

        //se (não há cache) OU (é o primeiro update) OU (now > hora prevista do update <=> (está na hora) OU (algo correu mal e corrige-se agora) OU (estamos a voltar à página já depois do último update e por isso é para fazer update) ) OU (não há valores guardados)
        if(!localStorage || !this.nextUpdate || now > this.nextUpdate || !storedValues) {
            this.items.forEach(function(item){
                var itemIndex = me.items.indexOf(item);
                var request = me.buildItemRequest(item);

                if(request === undefined){
                    return;
                }

                $.ajax({
                    dataType: "json",
                    xhrFields: { withCredentials: true },
                    url: request.url,
                    data: request.data,
                    type: request.type,
                    ignoreErrors: true,
                    success: function(data){
                        me.values[itemIndex] = me.parseQueryResult(itemIndex,data);
                        if(localStorage) localStorage.setItem(me.localStorageKey+'Values', JSON.stringify(me.values));
                        me.updateItem(itemIndex);
                    }
                });
            });
            this.nextUpdate = now + this.interval;
            if(localStorage) localStorage.setItem(this.localStorageKey+"NextUpdate",this.nextUpdate);
        } else { // caso contrário (update já foi feito por outra página) OU (estamos a voltar à página antes de ser necessário outro update) => usar valores antigos
            this.values = JSON.parse(storedValues);
            this.updateAllItems();
        }
        // Em qualquer dos casos é para manter o ciclo de actualização desta página (independentemente de quem acabará por fazer o trabalho, caso hajam vários dashboards em exibição)
        this.timeoutId = setTimeout(function(){
            me.launchUpdateDataCycle();
        }, this.interval);
    }
};

Dashboard.prototype.stopUpdateCycle = function() {
    clearTimeout(this.timeoutId);
};

// Funções necessárias definir nas implementações dos dashboards
Dashboard.prototype.buildHtml = function () {};
Dashboard.prototype.updateItem = function(item) {};
Dashboard.prototype.buildItemRequest = function (item) {};
Dashboard.prototype.parseQueryResult = function (itemIndex,data) {};

module.exports = Dashboard;